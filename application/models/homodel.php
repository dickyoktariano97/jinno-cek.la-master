<?php defined('BASEPATH') OR exit('No direct script access allowed');

class homodel extends CI_Model
{
    private $table = "tb_user";
    private $tablevisit = "tb_log_visitor";
    private $tablelink = "tb_link";

    public function doLogin($table,$where)
    {
        return $this->db->get_where($table,$where);
    }
    
    public function getById($id)
    {
        return $this->db->get_where($this->table, ["idUser" => $id])->row();
    }

    public function getAll()
    {
        return $this->db->get($this->table)->result();
	}
    
	public function save($data)
    {
        return $this->db->insert($this->table, $data);
    }

    public function update($data,$id)
    {
        return $this->db->update($this->table, $data, array('idUser' => $id));
    }

    public function delete($id)
    {
        return $this->db->delete($this->table, array("idUser" => $id));
    }

    // Visitor
    public function getByIdvisit($id)
    {
        return $this->db->get_where($this->tablevisit, ["idLog" => $id])->row();
    }

    public function getAllvisit()
    {
        return $this->db->get($this->tablevisit)->result();
	}

    // Link
    public function getByIdlink($id)
    {
        return $this->db->get_where($this->tablevisit, ["idLog" => $id])->row();
    }

    public function getAllLink()
    {
        return $this->db->get($this->tablevisit)->result();
	}

    //join
    public function data_akun(){
        $this->db->select('*');
        $this->db->from('tb_user');   
        $query = $this->db->get();
        return $query;
     }
     public function data_link(){
        $this->db->select('*');
        $this->db->from('tb_link');
        $this->db->join('user','tb_user.idUser = tb_link.idLink');      
        $query = $this->db->get();
        return $query;
     }
     function join2table(){
        $this->db->select('*');
        $this->db->from('tb_link');
        $this->db->join('tb_user','tb_user.idUser = tb_link.idLink');      
        $query = $this->db->get();
        return $query;
     }
}