<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Domain_model extends CI_Model
{
    private $_table = "tb_domain";

    public $idDomain; //autoIncrement
    public $domainName;
    public $status;

    public function rules()
    {
        return [

            ['field' => 'domainName',
            'rules' => 'required'],

            ['field' => 'status',
            'rules' => 'required']
        ];
    }

    public function getAll()
    {
        return $this->db->get($this->_table)->result();
    }

    public function getById($id)
    {
        return $this->db->get_where($this->_table, ["idDomain" => $id])->row();
    }

    // public function getAllMaintener($id)
    // {
    //     return $this->db->get_where($this->_table, ["idUser" => $id])->result();
    // }

    // public function cekstatus($status)
    // {
    //     return $this->db->get_where($this->_table, ["status" => $status])->num_rows();
    // }

    // public function save()
    // {
    //     $post = $this->input->post();
    //     $this->idDomain = "";
    //     $this->domainName = $post["domainName"];
    //     $this->status = preg_replace('/\s+/', '', $post["status"]);
    //     $this->idUser = $this->session->userdata('idUser');
    //     $this->campaignName = $post["campaignName"];
    //     return $this->db->insert($this->_table, $this);
    // }

    // public function update($id)
    // {
    //     $post = $this->input->post();
    //     $this->idDomain = $id;
    //     $this->domainName = $post["domainName"];
    //     $this->status = preg_replace('/\s+/', '', $post["status"]);
    //     $this->idUser = $this->session->userdata('idUser');
    //     $this->campaignName = $post["campaignName"];
    //     return $this->db->update($this->_table, $this, array('idDomain' => $id));
    // }

    // public function delete($id)
    // {
    //     return $this->db->delete($this->_table, array("idDomain" => $id));
    // }

}
