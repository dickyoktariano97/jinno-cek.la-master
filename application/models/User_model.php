<?php defined('BASEPATH') OR exit('No direct script access allowed');

class User_model extends CI_Model
{

    public $idUser; //autoIncrement
    public $username;
    public $password;
    public $status;
    public $level; //1 = admin, 2 = user

    public function rules()
    {
        return [

            ['field' => 'username',
            'rules' => 'required'],

            ['field' => 'password',
            'rules' => 'required'],

            ['field' => 'status',
            'rules' => 'required'],

            ['field' => 'level',
            'rules' => 'required']
        ];
    }
    public function rulesEdit()
    {
        return [

            ['field' => 'username',
            'rules' => 'required'],


            ['field' => 'status',
            'rules' => 'required'],

            ['field' => 'level',
            'rules' => 'required']
        ];
    }

    public function getAll()
    {
        return $this->db->get('tb_user')->result();
    }

    public function getById($id)
    {
        return $this->db->get_where('tb_user', ["idUser" => $id])->row();
    }

    public function getByUsername($username)
    {
        return $this->db->get_where('tb_user', ["username" => $username])->result();
    }


    public function cekUsername($username)
    {
        return $this->db->get_where('tb_user', ["username" => $username])->num_rows();
    }

    public function saveManual()
    {

        $post = $this->input->post();
        $this->idUser = "";
        $this->username = $post['username'];
        $this->password = hash("sha512", md5($post["password"]));
        $this->status = $post['status'];
        $this->level = $post['level'];
        return $this->db->insert('tb_user', $this);
    }

    public function save()
    {
        $post = $this->input->post();
        $this->idUser = "";
        $this->username = $post['username'];
        $this->password = hash("sha512", md5($post["password"]));
        $this->status = 0;
        $this->level = 2;
        return $this->db->insert('tb_user', $this);
    }

    public function update($id)
    {
        $post = $this->input->post();

        // $password =  preg_replace('/\s+/', '', $post["password"]);

        $this->idUser = $id;
        $this->username = $post['username'];
        if($post['old_password'] == $post['password']){
            $this->password = $post["old_password"];
        }else{
            $this->password = hash("sha512", md5($post["password"]));
        }
        $this->status = $post['status'];
        $this->level = $post['level'];
        return $this->db->update('tb_user', $this, array('idUser' => $id));
    }


    public function delete($id)
    {
        return $this->db->delete('tb_user', array("idUser" => $id));
    }


}
