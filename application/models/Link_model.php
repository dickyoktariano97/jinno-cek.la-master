<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Link_model extends CI_Model
{
    private $_table = "tb_link";

    public $idLink; //autoIncrement
    public $redirectTo;
    public $linkEndPoint;
    public $idUser; //login
    public $campaignName;
    public $created_at; //timestamp
    public $idDomain;

    public function rules()
    {
        return [

            ['field' => 'redirectTo',
            'rules' => 'required'],

            ['field' => 'linkEndPoint',
            'rules' => 'required'],

            ['field' => 'campaignName',
            'rules' => 'required'],

            ['field' => 'idDomain',
            'rules' => 'required']
        ];
    }

    public function getAll()
    {
        return $this->db->get($this->_table)->result();
    }

    public function getById($id)
    {
        return $this->db->get_where($this->_table, ["idLink" => $id])->row();
    }

    public function getAllMaintener($id)
    {
        return $this->db->get_where($this->_table, ["idUser" => $id])->result();
    }

    public function cekLinkEndPoint($linkEndPoint)
    {
        return $this->db->get_where($this->_table, ["linkEndPoint" => $linkEndPoint])->num_rows();
    }

    public function save()
    {
        $post = $this->input->post();
        $this->idLink = "";
        $this->redirectTo = $post["redirectTo"];
        $this->linkEndPoint = preg_replace('/\s+/', '', $post["linkEndPoint"]);
        $this->idUser = $this->session->userdata('idUser');
        $this->campaignName = $post["campaignName"];
        $this->idDomain = $post["idDomain"];
        return $this->db->insert($this->_table, $this);
    }

    public function update($id)
    {
        $post = $this->input->post();
        $data_link = $this->getById($id);

        $this->idLink = $id;
        $this->redirectTo = $post["redirectTo"];
        $this->linkEndPoint = preg_replace('/\s+/', '', $post["linkEndPoint"]);
        $this->idUser = $this->session->userdata('idUser');
        $this->campaignName = $post["campaignName"];
        $this->created_at = $data_link->created_at;
        $this->idDomain = $post["idDomain"];
        return $this->db->update($this->_table, $this, array('idLink' => $id));
    }

    public function delete($id)
    {
        return $this->db->delete($this->_table, array("idLink" => $id));
    }

}
