<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Logvisitor_model extends CI_Model
{

    public function getAll()
    {
        return $this->db->get('tb_log_visitor')->result();
    }

    public function getById($id)
    {
        return $this->db->get_where('tb_log_visitor', ["idLog" => $id])->row();
    }
}
