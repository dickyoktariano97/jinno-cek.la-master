<!DOCTYPE html>
<html lang="en">
<?php
$this->load->view('parts/header');
?>
<body class="c-app c-dark-theme c-no-layout-transition">
  <div class="c-wrapper">
    <?php $this->load->view('parts/navbar'); ?>
    <div class="c-body">
      <main class="c-main">

       <div class="container-fluid">

        <div class="card">
          <div class="card-body">
           <div class="d-flex justify-content-between">
            <div>
             <h4 class="card-title mb-0">iLLOG</h4>
             <div class="small text-muted">a Dynamic IP Trapper</div>
           </div>
         </div>
       </div>
     </div>
   </div>
 </main>
</div>

</div>

<?php
$this->load->view('parts/footer.php');
?>

</body>
</html>
