<!DOCTYPE html>
<html lang="en">
<?php
$this->load->view('parts/header');
?>
<body class="c-app c-dark-theme c-no-layout-transition">
  <div class="c-wrapper">
    <div class="c-body">
      <main class="c-main">

        <body class="c-app c-dark-theme flex-row align-items-center" >
          <div class="container">
            <div class="row justify-content-center">
              <div class="col-md-8">
                <div class="card-group">
                  <div class="card p-4">
                    <div class="card-body">
                      <h1>Login</h1>
                      <p class="text-muted">Sign In to your account</p>
                      <form class="form-signin" action="<?= base_url('_radmin/account/login/loginPost');?>" method="post">
                        <input type="hidden" name="<?=$this->security->get_csrf_token_name();?>" value="<?=$this->security->get_csrf_hash();?>" style="display: none">

                        <div class="input-group mb-3">
                          <div class="input-group-prepend"><span class="input-group-text"><i class="fa fa-user"></i></span></div>
                          <input class="form-control" type="text" name="username" id="inputEmail" placeholder="Username" required="">
                        </div>
                        <div class="input-group mb-4">
                          <div class="input-group-prepend"><span class="input-group-text"><i class="fa fa-lock"></i></span></div>
                          <input class="form-control" type="password" id="inputPassword" name="password" placeholder="Password" required="">
                        </div>

                        <div class="row">
                          <div class="col-6">
                            <button class="btn btn-primary px-4" type="submit">Sign in</button>

                          </div>
                        </div>
                      </form>
                    </div>
                  </div>
                  <div class="card text-white bg-primary py-5 d-md-down-none" style="width:44%">
                    <div class="card-body text-justify">
                      <div>
                        <h2>iLLOG</h2>
                        <p>intellegnce visitor Logging APPS</p>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>


        </body>
      </main>
    </div>
  </div>

  <?php
  $this->load->view('parts/footer.php');
  ?>

</body>
</html>
