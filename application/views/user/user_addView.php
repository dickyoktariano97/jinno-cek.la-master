<!DOCTYPE html>
<html lang="en">
<?php
$this->load->view('parts/header');
?>

<body class="c-app c-dark-theme c-no-layout-transition">
  <div class="c-wrapper">
    <?php $this->load->view('parts/navbar'); ?>
    <div class="c-body">
      <main class="c-main">

       <div class="container-fluid">
        <div class="row">
          <div class="col-12" style="margin-bottom: 20px;">
            <a href="<?php echo base_url('_radmin/User'); ?>" class="pull-left"><button class="btn btn-sm btn-warning"><i class="fa fa-chevron-left"></i> Kembali</button></a>
          </div></div>

          <div class="card">

            <div class="card-header">

              <div class="d-flex justify-content-between">
                <div>
                  <h4 class="card-title mb-0">Tambah User</h4>
                </div>
              </div>
            </div>

            <div class="card-body">

              <form class="col-4"  action="<?= base_url('_radmin/User/add');?>" method="post" >
                <input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>">


                <div class="form-group">
                  <label >Username</label>
                  <input type="text" name="username" class="form-control" required>
                </div>

                <div class="form-group">
                  <label >Password</label>
                  <input type="password" name="password" class="form-control" required>
                </div>

                <div class="form-group">
                  <label >Status</label>
                  <select name="status" class="form-control" required="">
                    <option value="0">tidak aktif</option>
                    <option value="1">aktif</option>
                  </select>
                </div>

                <div class="form-group">
                  <label >Level</label>
                  <select name="level" class="form-control" required="">
                    <option value="1">admin</option>
                    <option value="2">user</option>
                  </select>
                </div>

                <br>

                <input type="submit" class="btn btn-info btn-md" value="Simpan" name="">
                <a href="<?php echo base_url('_radmin/User'); ?>" class="btn btn-danger btn-md" value="" name="">Batal</a>

              </form>

            </div>
          </div>
        </main>
      </div>

    </div>

    <?php
    $this->load->view('parts/footer.php');
    ?>

  </body>
  </html>
