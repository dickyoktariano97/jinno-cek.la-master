<!DOCTYPE html>
<html lang="en">
<?php
$this->load->view('parts/header');
?>

<body class="c-app c-dark-theme c-no-layout-transition">
  <div class="c-wrapper">
    <?php $this->load->view('parts/navbar'); ?>
    <div class="c-body">
      <main class="c-main">

       <div class="container-fluid">
        <?php if ($this->session->flashdata('success')): ?>
          <div class="alert alert-success" role="alert">
            <?php echo $this->session->flashdata('success'); ?>
          </div>
        <?php endif; ?>
        <div class="card">
          <div class="card-header">

            <div class="d-flex justify-content-between">
              <div>
                <h4 class="card-title mb-0">User Database</h4>
                <div class="small text-muted">User database to maintain the app</div>
              </div>
              <div class="btn-toolbar d-none d-md-block" role="toolbar" aria-label="Toolbar with buttons">
                <a href="<?php echo base_url('_radmin/User/add'); ?>" class="pull-right"><button class="btn btn-warning"><i class="fa fa-plus"></i> Tambah Data User</button></a>

              </div>
            </div>
          </div>

          <div class="card-body"  style="overflow: auto;">



            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
              <thead>
                <tr>
                  <th>Username</th>
                  <th>Status</th>
                  <th>Level</th>
                  <th>#</th>
                </tr>
              </thead>
              <tbody>
                <?php
                $cek_total_isi = count($users);
                if($cek_total_isi > 0){
                  foreach($users as $isi_user){
                    ?>
                    <tr>
                      <td><?php xssprint($isi_user->username); ?></td>
                      <td><?php
                      if($isi_user->status=='1'){
                        echo "<label class='badge badge-success'>Active</label>";
                      }else{
                        echo "<label class='badge badge-danger'>Inactive</label>";
                      }
                      ?></td>
                      <td>
                        <?php
                        if($isi_user->level=='1'){
                          echo "<label class='badge badge-info'>1 - Administrator</label>";
                        }else{
                          echo "<label class='badge badge-warning'>2 - Maintener</label>";
                        }
                        ?>
                      </td>
                      <td>
                        <a onclick="return confirm('Are you sure?')"  href="<?php echo site_url('_radmin/User/delete/'.encrypt_url($isi_user->idUser)) ?>"><button class="btn btn-sm btn-danger"><i class="fas fa-trash"></i></button></a>
                        <a  href="<?php echo site_url('_radmin/User/edit/'.encrypt_url($isi_user->idUser)) ?>"><button class="btn btn-sm btn-info"><i class="fa fa-edit"></i></button></a>

                      </td>
                    </tr>
                    <?php
                  }
                }
                ?>

              </tbody>
            </table>
          </div>
        </div>
      </main>
    </div>

  </div>

  <?php
  $this->load->view('parts/footer.php');
  ?>

</body>
</html>
