<!DOCTYPE html>

<html lang="en">
<!-- <head>
	
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<meta name="description" content="">
	<meta name="author" content="">
	<link rel="icon" href="../../../../favicon.ico">

	<title> CekLa URL Shortener </title>

	<!- Bootstrap core CSS ->
	<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/css/bootstrap.min.css" integrity="sha384-B0vP5xmATw1+K9KRQjQERJvTumQW0nPEzvF6L/Z6nronJ3oUOFUFpCjEUQouq2+l" crossorigin="anonymous">

	<!- Custom styles for this template ->
	<link href="navbar-top.css" rel="stylesheet">

</head> -->
<body>	
	<nav class="navbar navbar-expand-md navbar-dark bg-dark mb-4">
		<a class="navbar-brand">CekLa (URL Shortener)</a>
		<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
			<span class="navbar-toggler-icon"></span>
		</button>

		<div class="collapse navbar-collapse" id="navbarCollapse">
			<ul class="navbar-nav mr-auto">
				<li class="nav-item">
					<a class="nav-link" href="<?= base_url('dashboard/Home') ?>">Shortener <span class="sr-only">(current)</span></a>
				</li>
				<li class="nav-item active">
					<a class="nav-link" href="<?= base_url('dashboard/Register')?>">Register</a>
				</li>
				</li>
				<li class="nav-item">
					<a class="nav-link" href="<?= base_url('dashboard/V_akun')?>">View akun</a>
				</li>
				<li class="nav-item">
				<a class="nav-link" href="<?= base_url('dashboard/login') ?>">Logout</a>
				<!-- <a class="nav-link" href="<?= base_url('_radmin/account/login/loginPost') ?>">Login</a> -->
				</li>
			</ul>
		</div>
	</div>
</nav>
<div class="c-app c-dark-theme c-no-layout-transition">
<div class="container">
		<div class="card">
		<div class="card-header" style="text-align: center">
                   <h1><i class="fas fa-list-ul"></i> Registrasi Cekla</h1></div>
			<div class="card-body">
			<!-- <?php 
			if(validation_errors() != false)
			{
				?>
				<div class="alert alert-danger" role="alert">
					<?php echo validation_errors(); ?>
				</div>
				<?php
			}
			?> -->
			<form method="post" action="<?php echo base_url(); ?>dashboard/register/save">
				<div class="form-group">

					<label for="username">Email</label>
					<input type="text" class="form-control" id="username" placeholder="Email" name="username" required>
				</div>
				<div class="form-group">
					<label for="password">Password</label>
					<input type="password" class="form-control" id="password" placeholder="Password" name="password" required>
				</div>
				<div class="form-group">
					<label for="password">Konfirmasi Password</label>
					<input type="password" class="form-control" id="password2" name="password2" placeholder="Konfirmasi Password" required>
				</div>
				<div class="form-group">
				<label for="status"></label>
					 <input type="hidden" disabled="disabled" class="form-control" id="status" name="status" value="1" >
				</div>
				<div class="form-group">
					<label for="level"></label>
					 <input type="hidden" disabled="disabled" class="form-control" id="level" name="level" value="1">
					<!-- <select name='level'>
					<option value='1'selected='selected'>Administrator</option>
					<option value='2'>Maintener</option>
	 				</select> -->

				 <?php if(validation_errors()) { echo validation_errors(); }?>
   				<form action="<?php echo base_url('dashboard/register/check_captcha');?>" method="post">
				<tr><?php echo $captcha;?></tr>
				<!- <input type="submit"  class="fas fa-sync-alt" onClick="document.location.reload(true)">
				<div>
				<td>Masukan kode captcha</td>
				</div>
				<div>
				<input type="text" name="captcha">
				</div> 
				

				<!-- <div class="form-group">
				 <label for="created_at">Tanggal dibuat</label> -->
					<input type="hidden" name="tanggal" value="<?php echo date("d-m-Y"); ?>">
					<!-- <input type="text" class="form-control" name="created_at" id="created_at">
				</div> -->
				
				
				<td><button type="submit" name="tambah" id="btnSubmit" class="btn btn-primary float-right">Save</button></td>
				

				<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
    			<script type="text/javascript">
       			 $(function () {
        	     $("#btnSubmit").click(function () {
                var password = $("#password").val();
                var confirmPassword = $("#password2").val();
                if (password != confirmPassword) {
                    alert("Passwords do not match.");
                    return false;
					}
					return true;
					});
					});
				</script>
				<script>
				function reloadpage()
				{
				location.reload()
				}
				</script>
			</form>
			<!-- <?php
			$tanggal = "";

			if ($_SERVER["REQUEST_METHOD"] == "POST") {
				$tanggal = $_POST["tanggal"];

				echo "<h2>Hasil:</h2>";
				echo "Tanggal Sekarang : ".$tanggal;
			}
			?> -->
				</div>
		</div>
		</div>
	</div>
	</body>
	</html>