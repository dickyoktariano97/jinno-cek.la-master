<!DOCTYPE html>

<!-- <head>

	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<meta name="description" content="">
	<meta name="author" content="">
	<link rel="icon" href="../../../../favicon.ico">


	<title> CekLa URL Shortener </title>

	<!- Bootstrap core CSS->
	<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/css/bootstrap.min.css" integrity="sha384-B0vP5xmATw1+K9KRQjQERJvTumQW0nPEzvF6L/Z6nronJ3oUOFUFpCjEUQouq2+l" crossorigin="anonymous">

	<!- Custom styles for this template ->
	<link href="navbar-top.css" rel="stylesheet">

</head> -->

<body>	
	<nav class="navbar navbar-expand-md navbar-dark bg-dark mb-4">
		<a class="navbar-brand">CekLa (URL Shortener)</a>
		<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
			<span class="navbar-toggler-icon"></span>
		</button>

		<div class="collapse navbar-collapse" id="navbarCollapse">
			<ul class="navbar-nav mr-auto">
				<li class="nav-item active">
					<a class="nav-link" href="<?= base_url('dashboard/Home') ?>">Shortener <span class="sr-only">(current)</span></a>
				</li>
				<li class="nav-item">
					<a class="nav-link" href="<?= base_url('dashboard/Register')?>">Register</a>
				</li>
				</li>
				<li class="nav-item">
					<a class="nav-link" href="<?= base_url('dashboard/V_akun')?>">View akun</a>
				</li>
				<li class="nav-item">
				<a class="nav-link" href="<?= base_url('dashboard/login') ?>">Logout</a>
				<!-- <a class="nav-link" href="<?= base_url('_radmin/account/login/loginPost') ?>">Login</a> -->
				</li>
			</ul>
		</div>
	</div>
</nav>
<div class="c-app c-no-layout-transition">
<div class="container">
    <table width="300px">
        <div class="card">
                <div class="card-header" style="text-align: center">
				<!-- <table class="table" style="text-align: center"> -->
				<h1><i class="fas fa-link"></i> URL Shortener - Get short link</h1>
                </div>
            
                <div class="input-group mb-3">
                    <input type="text" class="form-control">
                    <div class="input-group-append">
                    <button class="btn btn-primary" type="submit">Shorten URL</button>
                    <th></th>
                    <th></th>
                    <th></th>
                    <tr></tr>
                 </div>

				<div class="form-group">
					<label for="namaPetugas">Short Link</label>
					<input type="text" class="form-control" id="namaPetugas" name="namaPetugas">
				</div>

				</div>
        </div>
</div>

</table>
    	<!-- Optional JavaScript -->
		<!-- jQuery first, then Popper.js, then Bootstrap JS -->
		<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
		<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous"></script>
		<script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/js/bootstrap.min.js" integrity="sha384-+YQ4JLhjyBLPDQt//I+STsc9iw4uQqACwlvpslubQzn4u2UU2UFM80nGisd026JF" crossorigin="anonymous"></script>
		<!-- <script src="<?= base_url(); ?>assets/js/sweetalert2.all.min.js"></script>
   		<script src="<?= base_url(); ?>assets/js/myscript.js"></script> -->
		   <!-- Fontawesome JS -->
		<script src="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.13.0/js/all.min.js"></script>
	</body>

	<div class="container" style="text-align:center;">
	  	<hr/>
        <span>CEKLA @<?php echo date('Y'); ?></span>
      </div>
</html>