<!DOCTYPE html>
<html lang="en">
<?php
$this->load->view('parts/header');
?>
	<!-- <head>
		<!- Required meta tags ->
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		<title>CekLa URL Shortener</title>

		<!- Bootstrap CSS ->
		<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/css/bootstrap.min.css" integrity="sha384-B0vP5xmATw1+K9KRQjQERJvTumQW0nPEzvF6L/Z6nronJ3oUOFUFpCjEUQouq2+l" crossorigin="anonymous">
	</head> -->

	<body class="c-app c-dark-theme c-no-layout-transition">
	<div class="container">
    <table width="300px">
    <div class="card">
	<div class="card-header" style="text-align: center">
	<!-- <table class="table" style="text-align: center"> -->
	<h1><i class="fas fa-sign-in-alt"></i> Login CEKLA</h1>
        </div>
		<form action="<?= site_url('dashboard/login/func_login') ?>" method="POST">
			<div class="md-12">
				<table class="table" style="text-align: center;">
					<tr>
						<td>Username</td>
						<td>:</td>
						<td><input type="text" class="form-control" name="username" placeholder="Input Username" maxlength="50" required>
		      
						</td>
					</tr>
					<tr>
						<td>Password</td>
						<td>:</td>
						<td><input type="password" class="form-control" name="password" placeholder="Input Password" maxlength="50" required></td>
					</tr>
					<tr>
						<td></td>
						<td></td>
						<td><input type="submit" class="btn btn-primary" value="LOGIN" name="login"></td>
					</tr>
				</table>
			</div>
			</div>
			</div>
		</form>
		<!-- Optional JavaScript -->
		<!-- jQuery first, then Popper.js, then Bootstrap JS -->
		<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
		<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous"></script>
		<script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/js/bootstrap.min.js" integrity="sha384-+YQ4JLhjyBLPDQt//I+STsc9iw4uQqACwlvpslubQzn4u2UU2UFM80nGisd026JF" crossorigin="anonymous"></script></body>
		<?php
		$this->load->view('parts/footer.php');
		?>
</html>