
	<nav class="navbar navbar-expand-md navbar-dark bg-dark mb-4">
		<a class="navbar-brand">CekLa (URL Shortener)</a>
		<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
			<span class="navbar-toggler-icon"></span>
		</button>

		<div class="collapse navbar-collapse" id="navbarCollapse">
			<ul class="navbar-nav mr-auto">
				<li class="nav-item ">
					<a class="nav-link" href="<?= base_url('dashboard/Home') ?>">Shortener <span class="sr-only">(current)</span></a>
				</li>
				<li class="nav-item">
					<a class="nav-link" href="<?= base_url('dashboard/Register')?>">Register</a>
				</li>
				<li class="nav-item active">
					<a class="nav-link" href="<?= base_url('dashboard/v_akun')?>">View akun</a>
				</li>
				<li class="nav-item">
				<a class="nav-link" href="<?= base_url('dashboard/login') ?>">Logout</a>
				<!-- <a class="nav-link" href="<?= base_url('_radmin/account/login/loginPost') ?>">Login</a> -->
				</li>
			</ul>
		</div>
	</div>
</nav>	
<h1><i style="text-align: center" class="fas fa-user" ></i> Data User</h1>
<div class="c-dark-theme c-no-layout-transition">
	<div class="card">
		<div class="card-header" style="text-align: center">
		</div>
				<div class="card-body">
				<br/>
				<br/>
				<table class="table table-bordered" style="text-align: center">
					<tr>
						<th width="5%">No</th>
						<th>Nama</th>
						<td>Link</td> 
						<td>Campaign Name</td> 
						<!-- <th>Password</th> -->
						<!-- <th>Link</th> -->
					</tr>
					<?php 
					$no = 1;
					foreach($register as $row)
					// foreach($join_user_link as $rows)
					{
						?>
						<tr>
							<td width="5%"><?php echo $no++; ?></td>
							<td><?php echo $row->username; ?></td>
							<!-- <?php echo $rows->redirectTo ?></td>
							<td><?php echo $rows->campaignName ?></td> -->
							<td>
							<a href="<?php echo base_url(); ?>dashboard/V_akun/edit/<?php echo $row->idUser; ?>" class="btn btn-primary"><i class="fas fa-edit"></i>Edit User</a>
							</td>
							<td>
							<a href="<?php echo base_url(); ?>dashboard/V_akun/delete/<?php echo $row->idUser; ?>" class="btn btn-danger tombol-hapus"><i class="fas fa-trash-alt"></i>Hapus User</a>
							</td>						
						</tr>
						<?php
					}
					?>
					<?php 
					$no = 1;
					// foreach($register as $row)
					foreach($join_user_link as $rows)
					{
						?>
						<tr>
							<td width="5%"><?php echo $no++; ?></td>
							<!-- <td><?php echo $row->username; ?></td> -->
							<?php echo $rows->redirectTo ?></td>
							<td><?php echo $rows->campaignName ?></td>
							<td>
							<a href="<?php echo base_url(); ?>dashboard/V_akun/edit/<?php echo $row->idUser; ?>" class="btn btn-primary"><i class="fas fa-edit"></i>Edit User</a>
							</td>
							<td>
							<a href="<?php echo base_url(); ?>dashboard/V_akun/delete/<?php echo $row->idUser; ?>" class="btn btn-danger tombol-hapus"><i class="fas fa-trash-alt"></i>Hapus User</a>
							</td>						
						</tr>
						<?php
					}
					?>
				</table>
				
			</div>
		</div>
        </div>
</main>
