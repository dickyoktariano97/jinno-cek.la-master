<div class="container">
		<div class="card">
        <div class="card-header" style="text-align: center">
                   <h1><i class="fas fa-hospital-user"></i> Edit Akun</h1></div>
			<div class="card-body">
			<?php 
			if(validation_errors() != false)
			{
				?>
				<div class="alert alert-danger" role="alert">
					<?php echo validation_errors(); ?>
				</div>
				<?php
			}
			?>
			<form method="post" action="<?php echo base_url(); ?>/dashboard/V_akun/update/">
				<div class="form-group">

					<label for="username">Email</label>
					<input type="text" class="form-control" id="username" name="username" value="<?php echo $register->username; ?>">
				</div>

				<div class="form-group">
					<label for="password">Password</label>
					<input type="text" class="form-control" id="password" name="password"value="<?php echo $register->password; ?>">
				</div>

				<div class="form-group">
					<label for="level">Status</label>
					<input type="text" class="form-control" id="level" name="level"value="<?php echo $register->level; ?>">
				</div>

				<div class="form-group">
					<label for="status">Level</label>
					<input type="text" class="form-control datepicker" id="status" name="status"value="<?php echo $register->status; ?>">
				</div>

				<button type="submit" class="btn btn-primary">Update</button>
			</form>
			</div>
		</div>
	</div>


