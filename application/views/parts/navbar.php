


<header class="c-header c-header-light c-header-fixed">


 <ul class="c-header-nav ">
   <li class="c-header-nav-item px-3 d-md-down-none"><a class="c-header-nav-link" href="#"><b>iLLOG</b></a></li>

   <li class="c-header-nav-item dropdown  mx-2">
     <a class="c-header-nav-link" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">
      <i class="fa fa-database"></i>&nbsp;Database &nbsp;
    </a>
    <div class="dropdown-menu dropdown-menu-left dropdown-menu-lg pt-0">
      <a class="dropdown-item" href="<?= base_url('_radmin/Link'); ?>"><i class="fa fa-globe"></i>&nbsp;Link Database</a>
      <?php if($this->session->userdata('level')=='1'){ ?>
        <a class="dropdown-item" href="<?= base_url('_radmin/User'); ?>"><i class="fa fa-user"></i>&nbsp;User Database</a>
        <!-- <a class="dropdown-item" href="<?= base_url('_radmin/Log_visitor'); ?>"><i class="fa fa-user"></i>&nbsp;Log Visitor</a> -->
      <?php } ?>
    </div>
  </li>

</ul>

<ul class="c-header-nav mfs-auto"></ul>
<ul class="c-header-nav">
  <li class="c-header-nav-item dropdown">
    <a class="c-header-nav-link" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false" style="margin-right: 30px;">
      <?php echo $this->session->userdata('username'); ?>
    </a>
    <div class="dropdown-menu dropdown-menu-right pt-0" style="margin-right: 30px;">
      <a class="dropdown-item" href="<?= base_url('_radmin/account/Login/logout'); ?>">
       Logout
     </a>
   </div>
 </li>

</ul>

</header>
