<head>
  <!--<base href="./">-->
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">
  <meta name="description" content="iLLOG">
  <meta name="author" content="NONAME">
  <meta name="keyword" content="illog">
  <title>CEKLA</title>
  <link href="<?= base_url('public/dark/css/style.css'); ?>" rel="stylesheet">
  <link href="<?= base_url('public/dark/css/datatable.css'); ?>" rel="stylesheet">
  <link href="<?= base_url('public/dark/vendors/@coreui/chartjs/css/coreui-chartjs.css'); ?>" rel="stylesheet">
  <link rel="stylesheet" type="text/css" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
  <meta name="robots" content="noindex">

  <form action="<?= site_url('dashboard/login/logout') ?>" method="post">
			<div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
			<div class="modal-dialog" role="document">
				<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title" id="exampleModalLabel">Logout Aplikasi ?</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body">

				<h4>Are you sure want to Logout ??</h4>

				</div>
				<div class="modal-footer">
					<input type="hidden" name="product_id" class="productID">
					<button type="button" class="btn btn-secondary" data-dismiss="modal">No</button>
					<button type="submit" class="btn btn-primary">Yes</button>
				</div>
				</div>
			</div>
			</div>
</head>
