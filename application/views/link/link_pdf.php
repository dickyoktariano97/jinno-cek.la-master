<!DOCTYPE html>
<html lang="en">
<head>
  <style type="text/css">
 body {
    font-family: 'Roboto';
}
  </style>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">
  <meta name="description" content="iLLOG">
  <meta name="author" content="NONAME">
  <title>iLLOG</title>
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
  <link rel="stylesheet" type="text/css" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
  <meta name="robots" content="noindex">
</head>
<body>
  <div>
    <center>
      <h2>
        TIPIDTER SUBDIT III REPORT iLLOG
      </h2>
  <h4 class="card-title mb-0">Capmapign: <?php echo $link_info[0]['campaignName']; ?></h4>


    </center>

  <hr>
</div>
  <table class="table table-bordered" style="font-size: 10px; " border="1">
    <thead>
      <tr>
        <th>IP Address</th>
        <th>Region</th>
        <th>GPS Location</th>
        <th>ISP</th>
        <th>User Agent</th>
        <th>Refferal</th>
        <th>Visited Time</th>
        <!-- <th>Orientation</th> -->
        <th>Time Zone</th>
        <th>User Time</th>
        <th>Language</th>
        <!-- <th>Incognito</th> -->
        <th>Ad Blocker</th>
        <th>Screen Size</th>
        <th>Battery</th>
        <th>GPU</th>
        <th>Browser</th>
        <th>Operating System</th>
        <!-- <th>Device</th> -->
        <th>Touch Screen</th>
        <th>Platform</th>
        <th>Host Name</th>
      </tr>
    </thead>
    <tbody>
      <?php
      $no=1;
      $cek_total_isi = count($log);
      if($cek_total_isi > 0){
        foreach($log as $isi_log){
          ?>
          <tr>
            <td><?php xssprint($isi_log->ipAddress); ?></td>
            <td><?php xssprint($isi_log->cityRegionCountry); ?></td>
            <td><?php xssprint($isi_log->location); ?></td>
            <td><?php xssprint($isi_log->isp); ?></td>
            <td><?php xssprint($isi_log->userAgent); ?></td>
            <td><?php xssprint($isi_log->reff); ?></td>
            <td><?php xssprint($isi_log->createdAt); ?></td>

            <!-- <td><?php xssprint($isi_log->orientation); ?></td> -->
            <td><?php xssprint($isi_log->timezone); ?></td>
            <td><?php xssprint($isi_log->userTime); ?></td>
            <td><?php xssprint($isi_log->language); ?></td>
            <!-- <td><?php xssprint($isi_log->incognito); ?></td> -->
            <td><?php xssprint($isi_log->adBlocker); ?></td>
            <td><?php xssprint($isi_log->screenSize); ?></td>
            <td><?php xssprint($isi_log->batterey); ?></td>
            <td><?php xssprint($isi_log->gpu); ?></td>
            <td><?php xssprint($isi_log->browser); ?></td>
            <td><?php xssprint($isi_log->operatingSystem); ?></td>
            <!-- <td><?php xssprint($isi_log->device); ?></td> -->
            <td><?php xssprint($isi_log->touchscreen); ?></td>
            <td><?php xssprint($isi_log->platform); ?></td>
            <td><?php xssprint($isi_log->hostName); ?></td>
          </tr>

          <?php
        $no++;

        }
      }
      ?>

    </tbody>
  </table>
<div class="row">
  <center>
    <h5>REPORT BY iLLOG TIPIDTER SUBIT III</h5>
    <?= "Generated: ".date('d-m-Y h:i:s'); ?>
  </center>
  <footer class="c-footer" style="position: fixed;bottom: 0;width: 100%;">
    <p class="pull-right">Created by Illog</p>
  </footer>
</div>
</body>
</html>
