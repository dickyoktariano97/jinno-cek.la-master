<!DOCTYPE html>
<html lang="en">
<?php
$this->load->view('parts/header');
?>

<body class="c-app c-dark-theme c-no-layout-transition">
  <div class="c-wrapper">
    <?php $this->load->view('parts/navbar'); ?>
    <div class="c-body">
      <main class="c-main">

       <div class="container-fluid">
        <?php if ($this->session->flashdata('success')): ?>
          <div class="alert alert-success" role="alert">
            <?php echo $this->session->flashdata('success'); ?>
          </div>
        <?php endif; ?>
        <div class="card">
          <div class="card-header">

            <div class="d-flex justify-content-between">
              <div>
                <h4 class="card-title mb-0">Link Database</h4>
                <!-- <div class="small text-muted">percobaan language : <p id="language"></p></div> -->
                <div class="small text-muted">Endpoint and redirector list</div>
              </div>
              <div class="btn-toolbar d-none d-md-block" role="toolbar" aria-label="Toolbar with buttons">
                <a href="<?php echo base_url('_radmin/Link/add'); ?>" class="pull-right"><button class="btn btn-warning"><i class="fa fa-plus"></i> Tambah Data Link</button></a>

              </div>
            </div>
          </div>

          <div class="card-body" style="overflow: auto;">

            <table class="table table-striped table-bordered datatable" id="dataTable" width="100%" cellspacing="0">
              <thead>
                <tr>
                  <th>Meta Icon</th>
                  <th>Campaign</th>
                  <th>Redirect</th>
                  <th>End Point</th>
                  <th>Created At</th>
                  <th>#</th>
                </tr>
              </thead>
              <tbody>
                <?php
                $cek_total_isi = count($links);
                if($cek_total_isi > 0){
                  foreach($links as $isi_link){
                    ?>
                    <tr>
                      <td><img height="50" width="50" src="<?= base_url(); ?>images/link/<?= $isi_link->meta_icon; ?>"></td>
                      <td>
                        <?php xssprint($isi_link->campaignName); ?><br>
                        <small><i class="fa fa-user"></i>&nbsp;<?php
                        foreach($users as $isi_user):
                          if($isi_link->idUser == $isi_user->idUser){
                            xssprint($isi_user->username);
                          }
                        endforeach;
                        ?></small>
                      </td>
                      <td><?php xssprint($isi_link->redirectTo); ?></td>
                      <td><?php xssprint($isi_link->linkEndPoint); ?></td>
                      <td><?php xssprint($isi_link->created_at); ?></td>
                      <td>
                        <a onclick="return confirm('Are you sure?')"  href="<?php echo site_url('_radmin/Link/delete/'.encrypt_url($isi_link->idLink)) ?>"><button class="btn btn-sm btn-danger"><i class="fas fa-trash"></i></button></a>
                        <a  href="<?php echo site_url('_radmin/Link/edit/'.encrypt_url($isi_link->idLink)) ?>"><button class="btn btn-sm btn-info"><i class="fa fa-edit"></i></button></a>
                        <a  href="<?php echo site_url('_radmin/Link/details/'.encrypt_url($isi_link->idLink)) ?>"><button class="btn btn-sm btn-warning"><i class="fa fa-search"></i></button></a>

                      </td>
                    </tr>
                    <?php
                  }
                }
                ?>

              </tbody>
            </table>
          </div>
        </div>
      </main>
    </div>

  </div>

  <?php
  $this->load->view('parts/footer.php');
  ?>

</body>
<script type="text/javascript">
  $(document).ready(function() {
    $('#dataTable').DataTable().destroy();
    $('#dataTable').DataTable( {
      "order": [[ 3, "desc" ]]
    } );
  } );
</script>
</html>
