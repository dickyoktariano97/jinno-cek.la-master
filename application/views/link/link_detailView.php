<!DOCTYPE html>
<html lang="en">
<?php
$this->load->view('parts/header');
?>

<body class="c-app c-dark-theme c-no-layout-transition">
  <div class="c-wrapper">
    <?php $this->load->view('parts/navbar'); ?>
    <div class="c-body">
      <main class="c-main">

       <div class="container-fluid">

        <div class="row">
          <div class="col-12" style="margin-bottom: 20px;">
            <a href="<?php echo base_url('_radmin/Link/'); ?>" class="pull-left"><button class="btn btn-warning btn-sm"><i class="fa fa-chevron-left"></i>Kembali</button></a>
          </div>
        </div>

        <div class="card">
          <div class="card-header">

            <div class="d-flex justify-content-between">
              <div>
                <h4 class="card-title mb-0"><?php echo $link_info[0]['campaignName']; ?></h4>
                <div class=" text-muted">
                  <small>created by <b><?= $link_info[0]['username'] ?></b> on <b><?= date("d M Y - h:i:s", strtotime($link_info[0]['created_at'])); ?></b></small><br>
                  <small><i class="fa fa-globe"></i>&nbsp; endpoint <b><?= $domain->domainName.'/'.$link_info[0]['linkEndPoint'] ?></b> | <i class="fa fa-refresh"></i> redirect to <b><?= $link_info[0]['redirectTo']; ?></b></small><br>
                </div>
                <hr>
                <a href="<?php echo site_url('_radmin/Link/pdf/'.$this->uri->segment(4)) ?>" class="btn btn-info btn-sm" target="_blank"><i class="fa fa-file"></i> Export to PDF</a>
              </div>
              <div class="btn-toolbar d-none d-md-block" role="toolbar" aria-label="Toolbar with buttons">

              </div>
            </div>
          </div>

          <div class="card-body"  style="overflow: auto;">

            <table class="table table-striped table-bordered datatable table-hover" id="dataTable" width="100%" cellspacing="0">
              <thead>
                <tr>
                  <th nowrap="">Export</th>
                  <th>IP Address</th>
                  <th>Region</th>
                  <th>GPS Location</th>
                  <th>ISP</th>
                  <th>User Agent</th>
                  <th>Refferal</th>
                  <th>Visited Time</th>
                  <!-- <th>Orientation</th> -->
                  <th>Time Zone</th>
                  <th>User Time</th>
                  <th>Language</th>
                  <!-- <th>Incognito</th> -->
                  <th>Ad Blocker</th>
                  <th>Screen Size</th>
                  <th>Battery</th>
                  <th>GPU</th>
                  <th>Browser</th>
                  <th>Operating System</th>
                  <!-- <th>Device</th> -->
                  <th>Touch Screen</th>
                  <th>Platform</th>
                  <th>Host Name</th>
                  <th>Real Location</th>
                </tr>
              </thead>
              <tbody>
                <?php
                $no=1;
                $cek_total_isi = count($log);
                if($cek_total_isi > 0){

                  foreach($log as $isi_log){
                    ?>
                    <tr>
                      <td nowrap="">

<!-- The button used to copy the text -->
<button class="btn btn-sm btn-success" onclick="copyfunction<?=$no;?>()"><i class="fa fa-whatsapp"></i> Copy WA</button>
                        <textarea id="whatsappval<?=$no;?>" style="height: 1px; width: 1px; text-align: left; ">
==============================
*TIPIDTER SUBDIT III iLLOG REPORT*
*CAMPAIGN: <?= $link_info[0]['campaignName']; ?>*
*ENTRY: <?= $no;?>*
==============================
*IP Address* ```: <?= $isi_log->ipAddress; ?>```
*Region* ```: <?= $isi_log->cityRegionCountry; ?>```
*ISP* ```: <?= $isi_log->isp; ?>```
*Time* ```: <?= $isi_log->createdAt; ?>```
*User Time* ```: <?= $isi_log->userTime; ?>```
*Device/Platform* ```: <?= $isi_log->platform; ?>```
*Real Location*: <?php if($isi_log->real_location==',' || $isi_log->real_location==''){ echo "No location fetch"; }else{ ?> http://www.google.com/maps/place/<?= ($isi_log->real_location); ?> <?php } ?>


Pesan berikut merupakan cuplikan singkat. Informasi yang lebih lengkap dapat dilihat dalam laporan iLLOG terkait dalam bentuk PDF.
                        </textarea>
                      </td>
                      <td><?php xssprint($isi_log->ipAddress); ?></td>
                      <td><?php xssprint($isi_log->cityRegionCountry); ?></td>
                      <td><?php xssprint($isi_log->location); ?></td>
                      <td><?php xssprint($isi_log->isp); ?></td>
                      <td><?php xssprint($isi_log->userAgent); ?></td>
                      <td><?php xssprint($isi_log->reff); ?></td>
                      <td><?php xssprint($isi_log->createdAt); ?></td>

                      <!-- <td><?php xssprint($isi_log->orientation); ?></td> -->
                      <td><?php xssprint($isi_log->timezone); ?></td>
                      <td><?php xssprint($isi_log->userTime); ?></td>
                      <td><?php xssprint($isi_log->language); ?></td>
                      <!-- <td><?php xssprint($isi_log->incognito); ?></td> -->
                      <td><?php xssprint($isi_log->adBlocker); ?></td>
                      <td><?php xssprint($isi_log->screenSize); ?></td>
                      <td><?php xssprint($isi_log->batterey); ?></td>
                      <td><?php xssprint($isi_log->gpu); ?></td>
                      <td><?php xssprint($isi_log->browser); ?></td>
                      <td><?php xssprint($isi_log->operatingSystem); ?></td>
                      <!-- <td><?php xssprint($isi_log->device); ?></td> -->
                      <td><?php xssprint($isi_log->touchscreen); ?></td>
                      <td><?php xssprint($isi_log->platform); ?></td>
                      <td><?php xssprint($isi_log->hostName); ?></td>
                      <td><?php if($isi_log->real_location==',' || $isi_log->real_location==''){ echo "No location fetch"; }else{ ?><a href="http://www.google.com/maps/place/<?= ($isi_log->real_location); ?>" target="_blank">Click to Access Map</a><?php } ?></td>
                    </tr>



                    <script type="text/javascript">
                      function copyfunction<?=$no;?>() {
                        var copyText = document.getElementById("whatsappval<?=$no;?>");
                        copyText.select();
                        copyText.setSelectionRange(0, 99999);
                        document.execCommand("copy");
                      }
                    </script>
                    <?php
                  $no++;

                  }
                }
                ?>

              </tbody>
            </table>
          </div>
        </div>
      </main>
    </div>

  </div>

  <?php
  $this->load->view('parts/footer.php');
  ?>

</body>
</html>
