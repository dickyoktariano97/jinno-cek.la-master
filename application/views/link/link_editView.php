<!DOCTYPE html>
<html lang="en">
<?php
$this->load->view('parts/header');
?>

<style type="text/css">
  #imgView{  
      padding:5px;
      width: 100px;
      height: 100px;
  }
  .loadAnimate{
      animation:setAnimate ease 2.5s infinite;
  }
  @keyframes setAnimate{
      0%  {color: #000;}     
      50% {color: transparent;}
      99% {color: transparent;}
      100%{color: #000;}
  }
  .custom-file-label{
      cursor:pointer;
  }
</style>


<body class="c-app c-dark-theme c-no-layout-transition">
  <div class="c-wrapper">
    <?php $this->load->view('parts/navbar'); ?>
    <div class="c-body">
      <main class="c-main">

       <div class="container-fluid">
        <div class="row">
          <div class="col-12" style="margin-bottom: 20px;">
            <a href="<?php echo base_url('_radmin/Link/'); ?>" class="pull-left"><button class="btn btn-sm btn-warning"><i class="fa fa-chevron-left"></i> Kembali</button></a>
          </div></div>

          <div class="card">

            <div class="card-header">

              <div class="d-flex justify-content-between">
                <div>
                  <h4 class="card-title mb-0">Edit Link</h4>
                </div>
              </div>
            </div>

            <div class="card-body">

              <form class="col-4"  action="" method="post" enctype="multipart/form-data">
                <input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>">

                <div class="form-group">
                  <label >Campaign Name</label>
                  <input type="text" name="campaignName" class="form-control" value="<?php echo $link->campaignName ?>" required>
                </div>

                <div class="form-group">
                  <label >Redirect To</label>
                  <input type="url" name="redirectTo" class="form-control" value="<?php echo $link->redirectTo ?>" required>
                </div>

                <div class="form-group row">
                  <div class="col-6">
                    <label>Domain</label>
                    <select class="form-control" name="idDomain" required="">
                      <option value=""></option>
                      <?php
                      if(count($domain) > 0){
                        foreach ($domain as $domain_value) {
                          ?>
                          <option value="<?= $domain_value->idDomain ?>" <?php if($domain_value->idDomain == $link->idDomain){ ?> selected <?php } ?> ><?php xssprint($domain_value->domainName); ?></option>
                          <?php
                        }
                      }
                      ?>
                    </select>
                  </div>
                  <div class="col-6">
                   <label >Link End Point</label>

                   <div class="input-group mb-3">
                    <input type="text" name="linkEndPoint" class="form-control" value="<?php echo $link->linkEndPoint ?>" required>
                  </div>
                </div>
              </div>

              <label>Meta Configuration</label>
              <hr>

              <div class="form-group">
                <label >Meta Title</label>
                <input type="text" name="meta_title" class="form-control" value="<?php echo $link->meta_title ?>" required>
              </div>

              <div class="form-group">
                <label >Meta Description</label>
                <textarea name="meta_description" class="form-control" required=""><?php xssprint($link->meta_description) ?></textarea>
              </div>

              <div class="form-group">
                <label >Meta Author</label>
                <input type="text" name="meta_author" class="form-control" value="<?php echo $link->meta_author ?>" required>
              </div> 

              <div class="form-group">
                <label >Meta Icon</label>
                <br>
                <img src="<?= base_url('images/link/'.$link->meta_icon); ?>" id="imgView" class="card-img-top img-fluid">
                <!-- <img height="50" width="50" src="<?= base_url('images/link/'.$link->meta_icon); ?>"> -->
                <input type="file" name="meta_icon" id="inputFile" class="form-control">
              </div>

              <hr>


              <div class="form-group" style="display: none;">
                <label >User</label>
                <input type="text" name="idUser" class="form-control" value="<?php echo $user->username ?>" readonly="" required>
              </div>



              <br>

              <input type="submit" class="btn btn-info btn-md" value="Update" name="">
              <a href="<?php echo base_url('_radmin/Link'); ?>" class="btn btn-danger btn-md" value="" name="">Batal</a>

            </form>

          </div>
        </div>
      </main>
    </div>

  </div>

  <?php
  $this->load->view('parts/footer.php');
  ?>
  <script>
      $("#inputFile").change(function(event) {  
        fadeInAdd();
        getURL(this);    
      });

      $("#inputFile").on('click',function(event){
        fadeInAdd();
      });

      function getURL(input) {    
        if (input.files && input.files[0]) {   
          var reader = new FileReader();
          var filename = $("#inputFile").val();
          filename = filename.substring(filename.lastIndexOf('\\')+1);
          reader.onload = function(e) {
            debugger;      
            $('#imgView').attr('src', e.target.result);
            $('#imgView').hide();
            $('#imgView').fadeIn(500);      
            $('.custom-file-label').text(filename);             
          }
          reader.readAsDataURL(input.files[0]);    
        }
        $(".alert").removeClass("loadAnimate").hide();
      }

      function fadeInAdd(){
        fadeInAlert();  
      }
      function fadeInAlert(text){
        $(".alert").text(text).addClass("loadAnimate");  
      }
  </script>
</body>
</html>
