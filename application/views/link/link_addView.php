<!DOCTYPE html>
<html lang="en">
<?php
$this->load->view('parts/header');
?>

<body class="c-app c-dark-theme c-no-layout-transition">
  <div class="c-wrapper">
    <?php $this->load->view('parts/navbar'); ?>
    <div class="c-body">
      <main class="c-main">

       <div class="container-fluid">
        <div class="row">
          <div class="col-12" style="margin-bottom: 20px;">
            <a href="<?php echo base_url('_radmin/Link'); ?>" class="pull-left"><button class="btn btn-sm btn-warning"><i class="fa fa-chevron-left"></i> Kembali</button></a>
          </div></div>

          <div class="card">

            <div class="card-header">

              <div class="d-flex justify-content-between">
                <div>
                  <h4 class="card-title mb-0">Add Link</h4>
                </div>
              </div>
            </div>

            <div class="card-body">

              <form class="col-lg-4 col-xs-12"  action="<?= base_url('_radmin/Link/add');?>" method="post" enctype="multipart/form-data">
                <input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>">

                <div class="form-group">
                  <label >Campaign Name</label>
                  <input type="text" name="campaignName" class="form-control" required>
                </div>

                <div class="form-group">
                  <label >Redirect To</label>
                  <input type="url" name="redirectTo" class="form-control" required>
                </div>

                <div class="form-group row">
                  <div class="col-6">
                    <label>Domain</label>
                    <select class="form-control" name="idDomain" required="">
                      <option value=""></option>
                      <?php
                      if(count($domain) > 0){
                        foreach ($domain as $domain_value) {
                          ?>
                          <option value="<?= $domain_value->idDomain; ?>"><?php xssprint($domain_value->domainName); ?></option>
                          <?php
                        }
                      }
                      ?>
                      <?php ?>
                    </select>
                  </div>
                  <div class="col-6">
                   <label >Link End Point</label>
                   <div class="input-group mb-3">
                    <input type="text" name="linkEndPoint" class="form-control" required>
                  </div>
                </div>
              </div>

              <label>Meta Configuration</label>
              <hr>

              <div class="form-group">
                <label >Meta Title</label>
                <input type="text" name="meta_title" class="form-control" required>
              </div>

              <div class="form-group">
                <label >Meta Description</label>
                <textarea name="meta_description" class="form-control" required=""></textarea>
              </div>

              <div class="form-group">
                <label >Meta Author</label>
                <input type="text" name="meta_author" class="form-control" required>
              </div> 

              <div class="form-group">
                <label >Meta Icon</label>
                <input type="file" name="meta_icon" class="form-control" required="">
              </div>

              <hr>

              <div class="form-group" style="display: none;">
                <label >User</label>
                <input type="text" name="idUser" class="form-control" value="<?php echo $user->username ?>" readonly="" required>
              </div>


              <br>

              <input type="submit" class="btn btn-success btn-md" value="Simpan" name="">
              <a href="<?php echo base_url('_radmin/Link'); ?>" class="btn btn-danger btn-md" value="" name="">Batal</a>

            </form>

          </div>
        </div>
      </main>
    </div>

  </div>

  <?php
  $this->load->view('parts/footer.php');
  ?>

</body>
</html>
