<!DOCTYPE html>
<html lang="en">
<?php
$this->load->view('parts/header');
?>

<body class="c-app c-dark-theme c-no-layout-transition">
  <div class="c-wrapper">
    <?php $this->load->view('parts/navbar'); ?>
    <div class="c-body">
      <main class="c-main">

       <div class="container-fluid">

        <div class="row">
          <div class="col-12" style="margin-bottom: 20px;">
            <a href="<?php echo base_url('_radmin/Link/'); ?>" class="pull-left"><button class="btn btn-warning btn-sm"><i class="fa fa-chevron-left"></i>Kembali</button></a>
          </div>
        </div>

        <div class="card">
          <div class="card-header">

            <div class="d-flex justify-content-between">
              <div>
                <h4 class="card-title mb-0">Log Visitor</h4>
                <div class=" text-muted">
            <!--       <small>created by <b><?= $link_info[0]['username'] ?></b> on <b><?= date("d M Y - h:i:s", strtotime($link_info[0]['created_at'])); ?></b></small><br>
                  <small><i class="fa fa-globe"></i>&nbsp; endpoint <b><?= base_url().$link_info[0]['linkEndPoint'] ?></b> | <i class="fa fa-refresh"></i> redirect to <b><?= $link_info[0]['redirectTo']; ?></b></small> --><br>
                </div>
              </div>
              <div class="btn-toolbar d-none d-md-block" role="toolbar" aria-label="Toolbar with buttons">

              </div>
            </div>
          </div>

          <div class="card-body"  style="overflow: auto;">

            <table class="table table-striped table-bordered datatable" id="dataTable" width="100%" cellspacing="0">
              <thead>
                <tr>
                  <th>IP Address</th>
                  <th>Region</th>
                  <th>GPS Location</th>
                  <th>ISP</th>
                  <th>User Agent</th>
                  <th>Refferal</th>
                  <th>Time</th>
                  <th>Orientation</th>
                  <th>Time Zone</th>
                  <th>User Time</th>
                  <th>Language</th>
                  <th>Incognito</th>
                  <th>Ad Blocker</th>
                  <th>Screen Size</th>
                  <th>Battery</th>
                  <th>GPU</th>
                  <th>Browser</th>
                  <th>Operating System</th>
                  <th>Device</th>
                  <th>Touch Screen</th>
                  <th>Platform</th>
                  <th>Host Name</th>
                </tr>
              </thead>
              <tbody>
                <?php
                $cek_total_isi = count($log_visitors);
                if($cek_total_isi > 0){
                  foreach($log_visitors as $isi_log){
                    ?>
                    <tr>
                      <td><?php xssprint($isi_log->ipAddress); ?></td>
                      <td><?php xssprint($isi_log->cityRegionCountry); ?></td>
                      <td><?php xssprint($isi_log->location); ?></td>
                      <td><?php xssprint($isi_log->isp); ?></td>
                      <td><?php xssprint($isi_log->userAgent); ?></td>
                      <td><?php xssprint($isi_log->reff); ?></td>
                      <td><?php xssprint($isi_log->createdAt); ?></td>

                      <td><?php xssprint($isi_log->orientation); ?></td>
                      <td><?php xssprint($isi_log->timezone); ?></td>
                      <td><?php xssprint($isi_log->userTime); ?></td>
                      <td><?php xssprint($isi_log->language); ?></td>
                      <td><?php xssprint($isi_log->incognito); ?></td>
                      <td><?php xssprint($isi_log->adBlocker); ?></td>
                      <td><?php xssprint($isi_log->screenSize); ?></td>
                      <td><?php xssprint($isi_log->batterey); ?></td>
                      <td><?php xssprint($isi_log->gpu); ?></td>
                      <td><?php xssprint($isi_log->browser); ?></td>
                      <td><?php xssprint($isi_log->operatingSystem); ?></td>
                      <td><?php xssprint($isi_log->device); ?></td>
                      <td><?php xssprint($isi_log->touchscreen); ?></td>
                      <td><?php xssprint($isi_log->platform); ?></td>
                      <td><?php xssprint($isi_log->hostName); ?></td>
                    </tr>
                    <?php
                  }
                }
                ?>

              </tbody>
            </table>
          </div>
        </div>
      </main>
    </div>

  </div>

  <?php
  $this->load->view('parts/footer.php');
  ?>

</body>
</html>
