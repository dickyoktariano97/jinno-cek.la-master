<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {

	public function index($url)
	{



		$this->db->select('*')
		->from('tb_link')
		->where('linkEndPoint',$url);
		$data_link = $this->db->get()->row();

		if(empty($data_link)){
			$this->load->view('errors/html/error_404');
		}else{


		}

		$data['link'] = $data_link;
            // print_r($data['link']);
		$this->load->view('view_datauser', $data);
            // logData($data_link);


	}



	public function dataUser(){
		$all_data = array(
			'browser' => $_POST['browser'],
			'language' => $_POST['language'],
			'hostname' => $_POST['hostname'],
			'adBlock' => $_POST['adBlock'],
			'gpu' => $_POST['gpu'],
			'screenSize' => $_POST['screenSize'],
			'idLink' => $_POST['idLink'],
			'redirectLink' => $_POST['redirectLink'],
			'os' => $_POST['os'],
			'touchscreen' => $_POST['touchscreen'],
			'platform' => $_POST['platform'],
			'batre' => $_POST['batre'],
			'orientation' => $_POST['orientation'],
			'timezone' => $_POST['offset'],
			'userTime' => $_POST['uTime'],
		);

        // print_r($all_data);
		function GetIP()
		{
			if (getenv("HTTP_CLIENT_IP") && strcasecmp(getenv("HTTP_CLIENT_IP"), "unknown"))
				$ip = getenv("HTTP_CLIENT_IP");
			else if (getenv("HTTP_X_FORWARDED_FOR") && strcasecmp(getenv("HTTP_X_FORWARDED_FOR"), "unknown"))
				$ip = getenv("HTTP_X_FORWARDED_FOR");
			else if (getenv("REMOTE_ADDR") && strcasecmp(getenv("REMOTE_ADDR"), "unknown"))
				$ip = getenv("REMOTE_ADDR");
			else if (isset($_SERVER['REMOTE_ADDR']) && $_SERVER['REMOTE_ADDR'] && strcasecmp($_SERVER['REMOTE_ADDR'], "unknown"))
				$ip = $_SERVER['REMOTE_ADDR'];
			else
				$ip = "unknown";
			return($ip);
		}


		$ipLog = "information.txt";

		$cookie = $_SERVER['QUERY_STRING'];

		$register_globals = (bool) ini_get('register_gobals');

		if ($register_globals) $ip = getenv('REMOTE_ADDR');
		else $ip = GetIP();
		$rem_port = $_SERVER['REMOTE_PORT'];
		$user_agent = $_SERVER['HTTP_USER_AGENT'];
		if (isset( $_SERVER['METHOD'])){
			$rqst_method = $_SERVER['METHOD'];
		}
		else{
			$rqst_method = "null";
		}
		if (isset( $_SERVER['REMOTE_HOST'])) {
			$rem_host = $_SERVER['REMOTE_HOST'];
		}
		else{
			$rem_host = "null";
		}
		if (isset($_SERVER['HTTP_REFERER'])) {
			$referer = $_SERVER['HTTP_REFERER'];
		}
		else
		{
			$referer = "null";
		}
		$date=date ("Y/m/d G:i:s");

		$ip_details = json_decode(file_get_contents("http://ipinfo.io/{$ip}/json"));

		$ci =& get_instance();
		$datadb = array(
			'idLink' => $all_data['idLink'],
			'ipAddress' => $ip,
			'cityRegionCountry' => ''.$ip_details->city.'#'.$ip_details->region.'#'.$ip_details->country,
			'location' => $ip_details->loc,
			'isp' => $ip_details->org,
			'userAgent' => $user_agent,
			'other' => 'Host:'.$rem_host.'#Method:'.$rqst_method.'#Ref:'.$referer.'#Cookie:'.$cookie,
			'orientation' => $all_data['orientation'],
			'timezone' => $all_data['timezone'],
			'userTime' => $all_data['userTime'],
			'language' => $all_data['language'],
			'adBlocker' => $all_data['adBlock'],
			'screenSize' => $all_data['screenSize'],
			'batterey' => $all_data['batre'],
			'gpu' => $all_data['gpu'],
			'browser' => $all_data['browser'],
			'operatingSystem' => $all_data['os'],
            // 'device'
			'touchscreen' => $all_data['touchscreen'],
			'platform' => $all_data['platform'],
			'hostname' => $all_data['hostname'],
		);
		$savedb =  $ci->db->insert('tb_log_visitor', $datadb);
		if($savedb){
			echo json_encode($all_data['redirectLink']);
            // echo "<script>document.location.href='".$all_data['redirectLink']."'</script>";
		}else{
			echo json_encode('gagal');
		}
	}


}

/* End of file Home.php */
/* Location: ./application/controllers/Home.php */
