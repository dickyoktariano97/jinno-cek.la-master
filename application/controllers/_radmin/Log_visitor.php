<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Log_visitor extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        if($this->session->userdata('status') != 'login'){
            redirect('_radmin/account/Login');
        }
        $this->load->model("Logvisitor_model");
    }

    public function index()
    {
        $data['log_visitors'] = $this->Logvisitor_model->getAll();

        $this->load->view("log_visitor/logvisitor_view", $data);
    }
}
