<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class User extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        if($this->session->userdata('status') != 'login' || $this->session->userdata('level')!= '1'){
            session_destroy();
            redirect('_radmin/account/Login');
        }
        $this->load->model("User_model");
    }

    public function index()
    {
        $data['users'] = $this->User_model->getAll();
        $this->load->view("user/user_dataView", $data);
    }

    public function add()
    {
        $user = $this->User_model;

        $validation = $this->form_validation;
        $validation->set_rules($user->rules());

        $post = $this->input->post();
        if ($validation->run()) {
            $cek = $user->cekUsername($post['username']);
            if($cek > 0){
                echo "<script>alert('Username telah digunakan !')</script>";
                echo "<script>document.location.href='".base_url("User/add")."'</script>";
            }else{
                $user->saveManual();
                $this->session->set_flashdata('success', 'Data berhasil disimpan');
                redirect('_radmin/User');
            }

        }

        $this->load->view("user/user_addView");
    }

    public function edit($id = null)
    {
        if (!isset($id)) redirect('_radmin/User');

        $id = decrypt_url($id);

        $user = $this->User_model;

        $validation = $this->form_validation;
        $validation->set_rules($user->rulesEdit());

        $post = $this->input->post();
        if ($validation->run()) {
            $username = $post['username'];
            $cek = $this->db->query("SELECT * FROM tb_user WHERE idUser != '$id' AND username = '$username'")->num_rows();
            if($cek > 0){
                echo "<script>alert('Username telah digunakan !')</script>";
            }else{
                $user->update($id);
                $this->session->set_flashdata('success', 'Data berhasil diupdate');
                redirect('_radmin/User');
            }
        }

        $data["user"] = $user->getById($id);
        if (!$data["user"]) show_404();

        $this->load->view("user/user_editView", $data);
    }

    public function delete($id=null)
    {
        if (!isset($id)) show_404();

        $id = decrypt_url($id);

        if ($this->User_model->delete($id)) {
            $this->session->set_flashdata('success', 'Data berhasil dihapus');
            redirect('_radmin/User');
        }
    }
}
