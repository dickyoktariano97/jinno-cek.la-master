<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {

    public function index()
    {
        if($this->session->userdata('status') == 'login'){
          echo "<script>alert('Anda telah login')</script>";
          echo "<script>window.location.href='".base_url("_radmin/account/Dashboard")."'</script>";
      }
      $this->load->view('account/login');
  }

  public function logout(){
    $this->session->sess_destroy();
    redirect('_radmin/account/login');

}

function loginPost(){
    $username = $this->input->post('username');
    $password = hash("sha512", md5($this->input->post('password')));

    $this->db->select('*');
    $this->db->from('tb_user');
    $this->db->where('password', $password);
    $this->db->where('username', $username);
    $count_login = $this->db->get();
    $result_login = $count_login->result_array();
        // print_r($result_login);

    if($count_login->num_rows() > 0){

        $data_session = array(
            'username' => $username,
            'status' => "login",
            'idUser' => $result_login[0]['idUser'],
            'level' => $result_login[0]['level'],
            'dataUser' => array($result_login)
        );

        $this->session->set_userdata($data_session);

        redirect(base_url("_radmin/account/Dashboard"));

    }else{
        echo "<script>alert('Username dan password salah !')</script>";
        echo "<script>document.location.href='".base_url("_radmin/account/Login")."'</script>";
    }
}

}

/* End of file Login.php */
/* Location: .//Users/mmt/Library/Caches/com.binarynights.ForkLift-3/FileCache/AF881819-7635-45F6-A548-49A651872225/Login.php */
