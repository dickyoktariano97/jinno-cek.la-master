<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        if($this->session->userdata('status') != 'login'){
            redirect('_radmin/account/Login');
        }
    }


    public function index()
    {
        $data = '';
        $this->load->view('account/dashboard', $data, FALSE);
    }

    public function getTimeLine(){
// DATE_FORMAT("2017-06-15", "%M-%d %Y");
        // SELECT   FROM tb_order_detail
        $query = $this->db->select(' LEFT(tb_order_detail.startDate, 10) as start, LEFT(tb_order_detail.endDate, 10) as end, tb_order.poNumber as title, tb_inventory.inventoryName as namaInventory')
        ->from('tb_order_detail')
        ->join('tb_inventory','tb_inventory.inventoryId = tb_order_detail.inventoryId')
        ->join('tb_order','tb_order.idOrder = tb_order_detail.idOrder')
        ->where('tb_order.status !=', 'draft');
        $hasil = $query->get()->result_array();

        // echo json_encode($hasil);

        // echo "<br>";


        // echo $hasil[0]['start'];
        $tampung = '';

        foreach ($hasil as $key => $value) {
            $startDate =  str_replace('/', '-', $value['start']);
            $endDate =  str_replace('/', '-', $value['end']);

            $tampung .= '{"start":"'.date("Y-m-d", strtotime($startDate)).'","end":"'.date("Y-m-d", strtotime($endDate. "+1 days")).'","title":"PO: '.$value['title'].' / '.$value['namaInventory'].'"},';
        }
        $tampung = '['.$tampung.']';
        $tampung = substr($tampung, 0, -2) . ']';


        echo $tampung;

        // echo '[{"start":"2020-10-01","end":"2020-10-14","title":"2020-09-684125"},{"start":"2020-10-01","end":"2020-10-14","title":"2020-09-737636"}]';



    }

}

/* End of file Dashboard.php */
/* Location: .//Users/mmt/Library/Caches/com.binarynights.ForkLift-3/FileCache/085C6411-C9FA-4B2F-BF4F-D8609984E0C3/Dashboard.php */
