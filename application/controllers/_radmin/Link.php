<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Link extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        if($this->session->userdata('status') != 'login'){
            redirect('_radmin/account/Login');
        }
        $this->load->model("Link_model");
        $this->load->model("Domain_model");
        $this->load->model("User_model");
    }

    public function index()
    {
        $data['users'] = $this->User_model->getAll();
        $idUser = $this->session->userdata('idUser');
        $level = $this->session->userdata('level');

        if($level=='2'){
            $data["links"] = $this->Link_model->getAllMaintener($idUser);
        }else{
            $data["links"] = $this->Link_model->getAll();
        }


        $this->load->view("link/link_dataView", $data);
    }

    public function add()
    {
        $link = $this->Link_model;
        $data['user'] = $this->User_model->getById($this->session->userdata('idUser'));

        $data['domain'] = $this->Domain_model->getAll();

        $validation = $this->form_validation;
        $validation->set_rules($link->rules());

        if ($validation->run()) {
            $post = $this->input->post();
            $cek_linkEndPoint = $link->cekLinkEndPoint($post['linkEndPoint']);
            if($cek_linkEndPoint > 0){
                echo "<script>alert('Link End Point telah tersedia !')</script>";
                echo "<script>document.location.href='".base_url("_radmin/Link/add")."'</script>";
            }else{
                $nama_gambar = $_FILES['meta_icon']['name'];
                if($nama_gambar == ''){
                    echo "<script>alert('data tidak lengkap')</script>";
                    echo "<script>document.location.href='".base_url("_radmin/Link/add")."'</script>";
                }else{

                    if($_FILES['meta_icon']['size'] > 1000){
                        echo "<script>alert('Size melebihi batas (1 mb)')</script>";
                        echo "<script>document.location.href='".base_url("_radmin/Link/add")."'</script>";
                    }

                    $explodeFoto = explode(".", $nama_gambar);
                    $hitungArrayExplode = count($explodeFoto);
                    $formatFile = $explodeFoto[$hitungArrayExplode - 1];

                    if($formatFile == "png" || $formatFile == "jpeg" || $formatFile == "jpg"){
                        $link->save();
                        $this->session->set_flashdata('success', 'Data berhasil disimpan');
                        redirect('_radmin/Link');
                    }else{
                        echo "<script>alert('Format tidak diperbolehkan')</script>";
                        echo "<script>document.location.href='".base_url("_radmin/Link/add")."'</script>";
                    }//end cek format
                }//end cek file
            }//end cek link end point
        }//end validation

        $this->load->view("link/link_addView", $data);
    }

    public function edit($id = null)
    {
        if (!isset($id)) redirect('_radmin/Link');

        $id = decrypt_url($id);

        $link = $this->Link_model;
        $data['user'] = $this->User_model->getById($this->session->userdata('idUser'));

        $data['domain'] = $this->Domain_model->getAll();

        $validation = $this->form_validation;
        $validation->set_rules($link->rules());

        if ($validation->run()) {
            $post = $this->input->post();
            $linkEndPoint = $post['linkEndPoint'];
            $cek_linkEndPoint = $this->db->query("SELECT * FROM tb_link WHERE idLink != '$id' AND linkEndPoint = '$linkEndPoint'")->num_rows();
            if($cek_linkEndPoint > 0){
                echo "<script>alert('Link End Point telah tersedia !')</script>";
                echo "<script>document.location.href='".base_url("_radmin/Link/edit/".encrypt_url($id))."'</script>";
            }else{

                $meta_icon = "";
                $nama_gambar = $_FILES['meta_icon']['name'];
                if($nama_gambar == ''){
                    $link->update($id, $meta_icon);
                    $this->session->set_flashdata('success', 'Data berhasil diupdate');
                    redirect('_radmin/Link');
                }else{

                    if($_FILES['meta_icon']['size'] > 1000){
                        echo "<script>alert('Size melebihi batas (1 mb)')</script>";
                        echo "<script>document.location.href='".base_url("_radmin/Link/add")."'</script>";
                    }

                    $explodeFoto = explode(".", $nama_gambar);
                    $hitungArrayExplode = count($explodeFoto);
                    $formatFile = $explodeFoto[$hitungArrayExplode - 1];

                    if($formatFile == "png" || $formatFile == "jpeg" || $formatFile == "jpg"){
                        $meta_icon = "ada isinya";
                        $link->update($id, $meta_icon);
                        $this->session->set_flashdata('success', 'Data berhasil diupdate');
                        redirect('_radmin/Link');
                    }else{
                        echo "<script>alert('Format tidak diperbolehkan')</script>";
                        echo "<script>document.location.href='".base_url("_radmin/Link/add")."'</script>";
                    }//end cek format
                }//end cek file


                $link->update($id);
                $this->session->set_flashdata('success', 'Data berhasil diupdate');
                redirect('_radmin/Link');
            }

        }

        $data["link"] = $link->getById($id);
        if (!$data["link"]) show_404();

        $this->load->view("link/link_editView", $data);
    }

    public function details($id = null)
    {
        if (!isset($id)) redirect('_radmin/Link');

        $id = decrypt_url($id);

        $this->db->select("tb_link.*, tb_user.username");
        $this->db->from("tb_link");
        $this->db->join("tb_user","tb_user.idUser = tb_link.idUser");
        $this->db->where("tb_link.idLink",$id);
        $data['link_info'] = $this->db->get()->result_array();


        $this->db->select("*");
        $this->db->from("tb_log_visitor");
        $this->db->where('idLink',$id);
        $data['log'] = $this->db->get()->result();



        // print_r($data['log']);
        // echo "<br><br>";
        // print_r($data['link_info']);
        // echo "<br><br>";
        $data_link = $this->Link_model->getById($id);
        $data['domain'] = $this->Domain_model->getById($data_link->idDomain);
        $this->load->view("link/link_detailView", $data);
    }

    public function delete($id=null)
    {
        if (!isset($id)) show_404();

        $id = decrypt_url($id);

        if ($this->Link_model->delete($id)) {
            $this->session->set_flashdata('success', 'Data berhasil dihapus');
            redirect(site_url('_radmin/Link'));
        }
    }

    public function pdf($id = "")
    {
        if($id == ""){
            redirect(base_url()."_radmin/Link");
        }
        $id = decrypt_url($id);

        $this->db->select("tb_link.*, tb_user.username");
        $this->db->from("tb_link");
        $this->db->join("tb_user","tb_user.idUser = tb_link.idUser");
        $this->db->where("tb_link.idLink",$id);
        $data['link_info'] = $this->db->get()->result_array();

        $this->db->select("*");
        $this->db->from("tb_log_visitor");
        $this->db->where('idLink',$id);
        $data['log'] = $this->db->get()->result();


        $data_link = $this->Link_model->getById($id);
        $data['domain'] = $this->Domain_model->getById($data_link->idDomain);
        // $this->load->view("link/link_pdf", $data);

        $this->load->library('pdf');

        $nama_file =  "Link_Report_iLLOG_". date("Y-m-d");
        $this->pdf->generate('link/link_pdf', $data, $nama_file, 'A3', 'landscape');

    }
}
