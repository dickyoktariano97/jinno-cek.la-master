<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class V_akun extends CI_Controller {

    public function __construct()
    {
		parent::__construct();
		// if($this->session->userdata('status') != "login"){redirect(site_url("dashboard/Login"));}
        $this->load->model("homodel");
		$this->load->library('form_validation');
    }

    public function index()
	{
        $data['register'] = $this->homodel->getAll();
		$data['join_user_link'] = $this->homodel->join2table()->result(); 
        $this->load->view('parts/header');
        $this->load->view('home/akun',$data);
		$this->load->view('parts/footer');
       
	}

    function edit($idUser)
	{
		$data['register'] = $this->homodel->getById($idUser);
		$this->load->view('parts/header',$data);
		$this->load->view('home/edit',$data);
		$this->load->view('parts/footer');
	}

	public function update()
	{
		$this->form_validation->set_rules('username','Email','required');
		$this->form_validation->set_rules('password','Password','required');
		$this->form_validation->set_rules('status','Status','required');
		$this->form_validation->set_rules('level','Level','required');
		if ($this->form_validation->run()==true)
        {
			$idUser = $this->input->post('idUser');
            $data['username'] = $this->input->post('username');
			$data['password'] = $this->input->post('password');
			$data['level'] = $this->input->post('level');
			$data['status'] = $this->input->post('status');
			$this->homodel->update($data,$idUser);
			$this->session->set_flashdata('flash', 'Diubah');
			redirect('dashboard/v_akun');
		}
		else
		{
			$idUser = $this->input->post('idUser');
			$data['register'] = $this->homodel->getById($idUser);
            $this->load->view('parts/header');
            $this->load->view('home/edit');
            $this->load->view('parts/footer');
	    }
    }

    function delete($idUser)
	{
		$this->homodel->delete($idUser);
		$this->session->set_flashdata('flash', 'Dihapus');
		redirect('dashboard/v_akun');
	}

	public function data_akun(){
		$data['tb_user'] = $this->homodel->data_akun()->result(); 
		$data['tb_link'] = $this->homodel->data_link()->result(); 
		$data['join_user_link'] = $this->homodel->data_join2table()->result(); 
        $this->load->view('home/akun',$data);
	 } 
}