<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Short extends CI_Controller {

    public function index()
	{
        $this->load->view('parts/header');
        $this->load->view('home/short');
        $this->load->view('parts/footer');
	}
}