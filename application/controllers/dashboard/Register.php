<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Register extends CI_Controller{

	public function __construct()
    {
        parent::__construct();
        $this->load->model("homodel");
        $this->load->library('form_validation');
    }

	public function index()
	{
        $this->load->view('parts/header');
		$datac['captcha'] = $this->create_captcha();
		$this->load->view('home/register',$datac);
        $this->load->view('parts/footer');
		
	}
	

	public function save()
	{
		$this->form_validation->set_rules('username','Email','required');
		$this->form_validation->set_rules('password','Password','required');
		$this->form_validation->set_rules('status','Status','required');
		$this->form_validation->set_rules('level','Level','required');
		if ($this->form_validation->run()==true)

        {
			$data['username'] = $this->input->post('username');
			$data['password'] = $this->input->post('password');
			$data['level'] = $this->input->post('level');
			$data['status'] = $this->input->post('status');
			$this->homodel->save($data);
			$this->load->view('home/register');
		}
		else
		{
			$this->load->view('home/register');
		}
	}

	function create_captcha()
    {
        $datac = array(
            'img_path' => './captcha/',
            'img_url' => base_url('captcha'),
            'img_width' => '200',
            'img_height' => '70',
            'expiration' => 7200
        );
    
        $captcha = create_captcha($datac);
        $image = $captcha['image'];
    
        $this->session->set_userdata('captchaword', $captcha['word']);
    
        return $image;
    }

    function check_captcha()
    {
    if($this->input->post('captcha') == $this->session->userdata('captchaword')) {

        return true;
    
    } else {

        $this->form_validation->set_message('check_captcha', 'Captcha tidak sama');

        return false;
     }
    }

    // $this->load->library('email');

    // $this->email->from('your@example.com', 'Your Name');
    // $this->email->to('someone@example.com');
    // $this->email->cc('another@another-example.com');
    // $this->email->bcc('them@their-example.com');

    // $this->email->subject('Email Test');
    // $this->email->message('Testing the email class.');

    // $this->email->send();


}
