<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller{

	function __construct(){
		parent::__construct();		
		$this->load->model('homodel');
	}

	public function index(){
		$this->load->view('home/login');
	}

 
	public function func_login(){

		$username = $this->input->post('username');
		// $password = $this->input->post('password');
		$password = hash("sha512", md5($this->input->post('password')));
		$where = array(
			'username' => $username,
			'password' => $password
		);
		$cek = $this->homodel->doLogin("tb_user",$where)->num_rows();
		if($cek > 0){

    		$data_session = array(
				'nama' => $username,
				'status' => "login"
				);

			$this->session->set_userdata($data_session);
			$this->load->view('parts/header');
			$this->load->view('home/beranda');
			$this->load->view('parts/footer');
           

		}else{
			// $this->session->set_flashdata('error_msg', 'Username dan Password Salah');
			// echo validation_errors();
			echo ("<script LANGUAGE='JavaScript'>window.alert('Username or Password Wrong !');window.location.href='http://localhost/cekla/dashboard/login';</script>");;
		}
	}

	function logout(){
		$this->session->sess_destroy();
		$this->load->view('parts/header');
		$this->load->view('home/beranda');
		$this->load->view('parts/footer');
	}
}
?>