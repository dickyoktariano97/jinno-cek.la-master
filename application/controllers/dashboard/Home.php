<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {

    function __construct(){

		parent::__construct();		

		// if($this->session->userdata('status') != "login"){redirect(site_url("dashboard/Login"));}

		$this->load->model('homodel');

		$this->load->helper('url');
		// $this->load->helper('download'); // helper untuk fungsi download file

	}

    public function index()
	{
        $this->load->view('parts/header');
        $this->load->view('home/beranda');
        $this->load->view('parts/footer');
	}
}